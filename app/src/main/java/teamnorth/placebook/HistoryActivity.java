package teamnorth.placebook;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

/**
 * Created by Hunter on 6/3/2015.
 */
public class HistoryActivity extends ActionBarActivity implements ActionMode. Callback {
    private ArrayList <PlaceBookEntry> mPlacebookEntries = new ArrayList<PlaceBookEntry>();
    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super . onCreate ( savedInstanceState );

        Intent intent = getIntent () ;
        mPlacebookEntries = intent . getParcelableArrayListExtra ( MainActivity . VIEW_ALL_KEY );
// Populate the history list view .

    }
    @Override
    protected void onDestroy () {
        super . onDestroy () ;
        Intent intent = new Intent() ;
        intent . putParcelableArrayListExtra ( MainActivity . VIEW_ALL_KEY , mPlacebookEntries ) ;
        setResult ( Activity. RESULT_OK , intent ) ;
    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        return false;
    }

    @Override
    public void onDestroyActionMode(ActionMode actionMode) {

    }
}
