package teamnorth.placebook;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Hunter on 6/3/2015.
 */
public class PlaceBookEntry implements Parcelable {
    public long id ;
    private String name ;
    private String description ;
    private String photoPath ;


    public void PlaceBookEntry(Parcel source){
        this . id = source.readLong() ;
        this . name = source . readString () ;
        this . description = source . readString () ;
        this . photoPath = source . readString () ;




    }


    @Override
    public void writeToParcel ( Parcel dest , int flags ) {
        dest . writeLong ( this . id );
        dest . writeString ( this . name . toString () );
        dest . writeString ( this . description );
        dest . writeString ( this . photoPath );
    }
    @Override
    public int describeContents () {
        return 0;
    }
    public static final Parcelable . Creator < PlaceBookEntry > CREATOR
            = new Parcelable . Creator < PlaceBookEntry >() {
        @Override
        public PlaceBookEntry createFromParcel ( Parcel source ) {
            return new PlaceBookEntry ();
        }
        @Override
        public PlaceBookEntry [] newArray ( int size ) {
            return new PlaceBookEntry [ size ];
        }
    };
}


