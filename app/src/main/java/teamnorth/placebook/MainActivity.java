package teamnorth.placebook;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.drive.Drive;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlacePicker;

import java.io.File;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity implements ConnectionCallbacks, OnConnectionFailedListener {
    private static final int REQUEST_PLACE_PICKER = 1003;
    private GoogleApiClient mGoogleApiClient;
    private static final int REQUEST_IMAGE_CAPTURE = 1002;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    public static final String VIEW_ALL_KEY = " eecs40 . placebook . EXTRA_VIEW_ALL " ;
    private static final int REQUEST_VIEW_ALL = 1005;
    private ArrayList< PlaceBookEntry > mPlaceBookEntries = new ArrayList<PlaceBookEntry> ();


    // Call d i s p a t c h V i e w A l l P l a c e s () when its menu command is selected .
    private void dispatchViewAllPlaces () {
        Intent intent = new Intent ( this , HistoryActivity . class );
        intent . putParcelableArrayListExtra ( VIEW_ALL_KEY , mPlaceBookEntries );

        try {
            startActivityForResult ( intent , REQUEST_VIEW_ALL );
        } catch (  ActivityNotFoundException a ) {}
    }
    @Override
    protected void onActivityResult ( int requestCode , int resultCode , Intent data ) {
        if ( resultCode == RESULT_OK && requestCode == REQUEST_VIEW_ALL && data != null ) {
            ArrayList < PlaceBookEntry > placebookEntrys =
                    data . getParcelableArrayListExtra ( VIEW_ALL_KEY );
// Check if any entry was deleted .
        }
        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
        if (requestCode == REQUEST_PLACE_PICKER) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);
                String toastMsg = String.format("Place: %s", place.getName());
                Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();

            }

        }
    }

    // Call d i s p a t c h T a k e P i c t u r e I n t e n t () when the camera button is clicked .
    private void dispatchTakePictureIntent () {
        Intent takePictureIntent = new Intent ( MediaStore . ACTION_IMAGE_CAPTURE );
        Context context = getApplicationContext();
// Ensure that there 's a camera activity to handle the intent
        if ( takePictureIntent . resolveActivity ( getPackageManager () ) != null ) {
            File photoFile = new File("Users/Hunter/AndroidStudioProjects/PlaceBook/app/src/main/java/teamnorth/placebook/image");


        }
        else{
            Toast.makeText (this,"Camera Error", Toast.LENGTH_LONG). show () ;
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Create a GoogleApiClient instance
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Drive.API)
                .addScope(Drive.SCOPE_FILE)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        initGoogleApi();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super . onCreateOptionsMenu ( menu );



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch ( item . getItemId () ) {
            case R.id.action_settings: /* * Code to show settings * */
                ;
                return true;
            case R.id.action_new_place: /* * Code to add a new place * */
                launchPlacePicker ()
                ;

                return true;
            case R.id.action_view_all: /* * Code to show all places * */
                dispatchViewAllPlaces();

                return true;
            case R.id.button_location:
                ;
                return true;
            case R.id.button_snapshot:
                ;
                return true;
            case R.id.button_speak:
                ;
                return true;
            case R.id.button_place_picker:

                launchPlacePicker();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
        }
    // Call in itG oo gl eA pi () from MainActivity . onCreate ()
    private void initGoogleApi () {
        mGoogleApiClient = new GoogleApiClient
                . Builder ( this )
                . addApi ( Places . GEO_DATA_API )
                . addApi(Places.PLACE_DETECTION_API)
                . addConnectionCallbacks(this)
                . addOnConnectionFailedListener(this)
                . build() ;
    }


    // Call l a u n c h P l a c e P i c k e r () when the Pick -A - Place button is clicked .
    private void launchPlacePicker () {
        PlacePicker . IntentBuilder builder = new PlacePicker . IntentBuilder () ;
          Context context = getApplicationContext () ;
        try {
            startActivityForResult ( builder . build ( context ) , REQUEST_PLACE_PICKER );
        } catch ( GooglePlayServicesRepairableException e) {
// Handle exception - Display a Toast message
        } catch ( GooglePlayServicesNotAvailableException e ) {
// Handle exception - Display a Toast message

        }

    }





    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        // The good stuff goes here.
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the 'Handle Connection Failures' section.
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }
    }



    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }





    @Override
    protected void onStart() {
        super.onStart();
        if (!mResolvingError) {  // more about this later
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }



    /*A fragment to display an error dialog*/

    public static  class ErrorDialogFragment extends DialogFragment {
        // Request code to use when launching the resolution activity
        private static final int REQUEST_RESOLVE_ERROR = 1001;
        // Unique tag for the error dialog fragment
        private static final String DIALOG_ERROR = "dialog_error";
        public ErrorDialogFragment() { }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode,
                    this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((MainActivity)getActivity()).onDialogDismissed();
        }
    }

}
